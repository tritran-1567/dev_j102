stage = new createjs.Stage("demoCanvas");
circle = new createjs.Shape();
circle.graphics.beginFill("red").drawCircle(0, 0, 40);
circle.x = circle.y = 50;
stage.addChild(circle);
stage.update();

createjs.Ticker.addEventListener("tick", handleTick);
function handleTick() {
	circle.x += 10;
	if (circle.x > stage.canvas.width) {
		circle.x = 0;
	}
	stage.update();
}
