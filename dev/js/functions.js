// init
createjs.MotionGuidePlugin.install(createjs.Tween);
stage = new createjs.Stage("demoCanvas");
circle = new createjs.Shape();
circle1 = new createjs.Shape();
rect = new createjs.Shape();
var w = 30;
var w1 = 60;
// draw shape
circle.graphics
	.beginFill("#fff")
	.drawCircle(0, 0, w);
// set position
circle.x = circle.y = 42;
circle1.graphics
	.beginFill("#fff")
	.drawCircle(0, 0, w);
// set position
circle1.x = 80;
circle1.y = 42;
rect.graphics
	.beginFill("#fff")
	.drawRect(0, 0, w1, w1);
// set position
rect.x = 61;
rect.y = 19;
rect.rotation = 45;
// add shape
stage.addChild(circle);
stage.addChild(circle1);
stage.addChild(rect);
stage.update();
